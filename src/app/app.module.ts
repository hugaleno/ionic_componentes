import { FabsPage } from './../pages/fab/fabs';
import { GridsPage } from './../pages/grids/grids';
import { PokemonTipoPipe } from './../pages/listas/pokemon-tipo.pipe';
import { ListasPage } from './../pages/listas/listas';
import { ButtonsPage } from './../pages/buttons/buttons';
import { AlertPage } from './../pages/alerts/alert';
import { ActionSheetPage } from './../pages/action-sheet/action-sheet';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SegmentsPage } from '../pages/segments/segments';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ActionSheetPage,
    AlertPage,
    ButtonsPage,
    ListasPage,
    PokemonTipoPipe,
    GridsPage,
    SegmentsPage,
    FabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ActionSheetPage,
    AlertPage,
    ButtonsPage,
    ListasPage,
    GridsPage,
    SegmentsPage,
    FabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
