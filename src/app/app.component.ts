import { FabsPage } from './../pages/fab/fabs';
import { SegmentsPage } from './../pages/segments/segments';
import { GridsPage } from './../pages/grids/grids';
import { ListasPage } from './../pages/listas/listas';
import { AlertPage } from './../pages/alerts/alert';
import { ActionSheetPage } from './../pages/action-sheet/action-sheet';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ButtonsPage } from '../pages/buttons/buttons';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage:any = FabsPage;
  titulo: string ="FabsPage";

  componentes: Array<{titulo:string, component:any}>;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    public menu: MenuController
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.componentes = [
      { titulo:'ActionSheet', component: ActionSheetPage },
      { titulo:'Alert', component: AlertPage },
      { titulo:'Botões', component: ButtonsPage },
      { titulo:'Listas', component: ListasPage },
      { titulo:'Grids', component: GridsPage },
      { titulo:'Segments', component: SegmentsPage },
      { titulo:'Fabs', component: FabsPage }
    ]
    
  }

  abrirPagina(pagina:any): void {
    this.menu.close();
    this.titulo = pagina.titulo;
    this.nav.setRoot(pagina.component);
  }
}

