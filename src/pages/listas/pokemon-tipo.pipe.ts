import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'pokemonTipoPipe'
})
export class PokemonTipoPipe implements PipeTransform {

    transform(pokemons: any[], tipo:string) {
       /*  return pokemons.filter( function(pokemon){
            return pokemon.attr == tipo
        }); */
        return pokemons.filter( pokemon => pokemon.attr == tipo);
    }
}