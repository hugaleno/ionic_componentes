import { Component } from '@angular/core';
@Component({
    selector: 'listas-page',
    templateUrl:'listas.html'
})
export class ListasPage {

pokemons:Array<{nome:string,img:string,desc:string, attr:string}>;

constructor(){
    this.pokemons = [
        {
            nome:'Bulbasaur',
            img:'assets/imgs/Bulbasaur.png',
            desc:'pokemon tipo grama',
            attr:'grama'
        },
        {
            nome:'Charmander',
            img:'assets/imgs/Charmander.png',
            desc:'pokemon tipo fogo',
            attr:'fogo'
        },
        {
            nome:'Squirtle',
            img:'assets/imgs/Squirtle.png',
            desc:'pokemon tipo agua',
            attr:'agua'
        },
        {
            nome:'Caterpie',
            img:'assets/imgs/Caterpie.png',
            desc:'pokemon tipo inseto',
            attr:'inseto'
        }
    ];
}

}