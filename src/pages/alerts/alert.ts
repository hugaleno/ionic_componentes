import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Component({
    selector: 'alert-page',
    templateUrl: 'alert.html'
})
export class AlertPage {

    constructor(
        public alertCtrl: AlertController
    ){}

    showAlert(): void {
        let alert = this.alertCtrl.create({
            title: 'Parabéns!!!',
            subTitle:'Você é o nosso '+
                'visitante de número 99999 e ganhou R$100.000',
            buttons: ['Ok']
        });
        alert.present();
    }

    showPrompt(): void {
        let alert = this.alertCtrl.create({
            title: 'Informações requeridas.',
            message:'Digite seu nome.',
            inputs:[
                {
                    name:'nome',
                    placeholder:'Jose'
                }
            ],
            buttons: [
                {
                    text:'Cancelar',
                    handler: () => {
                        console.log("Cancelou então tá cancelado!");
                        
                    }
                },
                {
                    text:'Salvar',
                    handler: (data) => {
                        console.log("Bem vindo " + data.nome);
                        console.log(`Bem vindo ${data.nome}`);
                    }
                }
            ]
        });
        alert.present();
    }

    showConfirm(): void {
        let alert = this.alertCtrl.create({
            title: 'Jogar fora barra de ouro?',
            message:'Vc realmente deseja '
                +'jogar fora essa barra de ouro?',
            buttons: [
                {
                    text:'Não, não estou louco!',
                    handler: () => {
                        console.log("Ele não está louco!");
                    }
                },
                {
                    text:'Sim, estou louco!',
                    handler: () => {
                        console.log("Ele está louco!");
                    }
                }
            ]
        });

        alert.present();
    }

    showRadio(): void {
        let radioAlert = this.alertCtrl.create();
        radioAlert.setTitle("Qual sua cor preferida?");
        radioAlert.addInput({
            type: 'radio',
            label:'Azul',
            value: 'azul',
            checked: true
        });
        radioAlert.addInput({
            type: 'radio',
            label:'Amarela',
            value:'amarela'
        });
        radioAlert.addButton("Cancelar");
        radioAlert.addButton({
            text:'Ok',
            handler: (data) => {
                console.log(data);                
            }
        });
        radioAlert.present();
    }
}