import { Component } from '@angular/core';
import { ActionSheetController } from 'ionic-angular';

@Component({
    selector:'action-sheet-page',
    templateUrl:'action-sheet.html'
})
export class ActionSheetPage {

    opcaoEscolhida: string;

    constructor(
        public actionSheetCtrl: ActionSheetController
    ){}

    abrirOpcoes(): void {
        let action = this.actionSheetCtrl.create({
            title:'Escolha sabiamente',
            buttons:[
                {
                    text:'Charmander',
                    handler: () => {
                        this.opcaoEscolhida = "Charmander";
                    }
                },
                {
                    text:'Bulbasaur',
                    handler: () => {
                        this.opcaoEscolhida = "Bulbasaur";
                    }
                },
                {
                    text:'Squirtle',
                    handler: () => {
                        this.opcaoEscolhida = "Squirtle";
                    }
                },
                {
                    text:'Caterpie',
                    handler: () => {
                        this.opcaoEscolhida = "Caterpie";
                    }
                },
                {
                    text:'Cancelar',
                    role:'cancel'
                }
            ]
        });
        action.present();
    }
}