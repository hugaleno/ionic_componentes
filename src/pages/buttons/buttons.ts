import { Component } from '@angular/core';
@Component({
    selector: 'buttons-page',
    templateUrl: 'buttons.html'
})
export class ButtonsPage {

    large: boolean = false;
    small: boolean = true;
    tamanho: string = "Pequeno";
    icone: string = "ios-star-outline";

    mudaBotao(): void {
        if (this.small) {
            this.small = false;
            this.tamanho = "Normal"
        } else if (!this.large && !this.small) {
            this.large = true;
            this.tamanho = "Grande"
        } else {
            this.large = false;
            this.small = true;
            this.tamanho = "Pequeno"
        }
    }

    favorita(): void {
        if( this.icone == "ios-star-outline"){
            this.icone="ios-star";
        }else{
            this.icone="ios-star-outline";
        }
    }
}